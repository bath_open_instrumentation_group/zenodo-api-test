# Zenodo API Test

The script tests and demonstrates the usage of the [Zenodo REST API](http://developers.zenodo.org/).
It uses the [Zenodo sandbox](https://sandbox.zenodo.org) to show how to:
- Create a new deposit, upload a ZIP archive, set basic metadata, and publish it.
- Create a new version of the deposit, replace the uploaded archive by a new version, and publish the new version.

## Install

The script requires Python >= 3.4 and uses the libraries [requests](http://docs.python-requests.org/en/master/) 
(*Apache License 2.0*).

Please clone this repository and install the [required 
dependencies](requirements.txt). We suggest that you install them in a virtual environment.

> The script has been used on Windows 7 with Python 3.6.6.

### Install on Windows

On Windows, the installation can be performed as follows:

```bash
cd zenodo-api-test
python -m venv env
env\Scripts\activate.bat
pip install -r requirements.txt
```

> Currently, there is a bug when activating a virtual environment on Windows operating
> systems. Please adapt your `env\Scripts\activate.bat` as suggested in the [bug report](https://bugs.python.org/issue34144)
> until the problem is fixed. 

### Install on Linux

On Linux, the installation can be performed as follows:

```bash
cd zenodo-api-test
python3.6 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Usage

Before you start, please prepare the following things:
- Create an account in the [Zenodo sandbox](https://sandbox.zenodo.org).
- Create a new personal access token with scopes `deposit:actions` and `deposit:write`.
- Copy the access token to the file `zenodo-api-test/zenodo-api-key.txt` (Just one line, Git ignores this file).

Then run the script as follows:

```bash
python zenodo-api-test.py
```

The script will create a new deposit named `TEST -- Example Zenodo API Upload -- TEST` which consists of two versions.

## License

The script code and the accompanying material is licensed under the terms of the [MIT License](LICENSE).
